'use strict';
const CPF = require("@fnando/cpf/dist/node");

let errors = [];

class Validation {
    constructor() {
        errors = [];
    }
    isRequired(value, message) {
        if (!value || value.length <= 0) {
            errors.push({
                message: message
            });
        }
    }
    hasMinLen(value, min, message) {
        if (!value || value.length < min) {
            errors.push({
                message: message
            });
        }
    }
    hasMaxLen(value, max, message) {
        if (!value || value.length > max) {
            errors.push({
                message: message
            });
        }
    }
    // IsFixedLenght
    IsFixedLength(value, len, message) {
        if (value.length !== len) {
            errors.push({
                message: message
            });
        }
    }
    // IsEmail
    isCPF(value, message) {
        if (!CPF.isValid(value)) {
            errors.push({
                message: message
            });
        }
    }
    clear() {
        errors = [];
    }
    isValid() {
        return errors.length === 0;
    }
    errors() {
        return errors;
    }
    isNumbersOnly(value, message) {
        value = value.toString().split('');
        for (let index = 0; index < value.length; index++) {
            const element = Number.parseInt(value[index]);
            if (!Number.isInteger(element)) {
                errors.push({
                    message: message
                });
                index = value.length;
            }
        }
    }
    isNotAfterToday(value, message) {
        var date = new Date(value);
        if (date.getTime() > (new Date()).getTime()) {
            errors.push({
                message: message
            });
        }
    }

    isDate(value, message) {
        value = Date.parse(value);
        if (Number.isNaN(value)) {
            errors.push({
                message: message
            });
        }
    }
}


module.exports = Validation;