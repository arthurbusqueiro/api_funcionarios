'use strict';
const fs = require('fs');
const Worker = require('../models/worker');

exports.readfile = async () => {
    var file = fs.readFileSync('funcionarios.txt', 'utf8', );
    var rows = file.split('\n');
    var workers = [];
    for (let index = 1; index < rows.length; index++) {
        if (rows[index].includes(';')) {
            const worker = rows[index].split(';');
            var _worker = new Worker();
            var date = worker[0].split('/');
            _worker.RegistrationDate = new Date(date[2], date[1] - 1, date[0]);
            _worker.Job = worker[1];
            _worker.CPF = worker[2];
            _worker.Name = worker[3];
            _worker.UF = worker[4];
            _worker.Salary = worker[5];
            _worker.Status = worker[6].replace('\r', '');
            workers.push(_worker);
        }
    }
    return workers;
};