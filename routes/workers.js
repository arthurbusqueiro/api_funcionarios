'use strict';

const express = require('express');

const router = express.Router();
const controller = require('../controllers/workers');

router.get('/name/:name', controller.getByName);
router.get('/cpf/:cpf', controller.getByCPF);
router.get('/job/:job', controller.getByJob);
router.get('/registration/:registration', controller.getByRegistration);
router.get('/uf/:uf', controller.countByUF);
router.get('/salary/:lowest/:highest', controller.getBySalary);
router.get('/salary/:lowest', controller.getBySalaryLowest);
router.get('/status/:status', controller.getByStatus);
router.post('/', controller.creatOrUpdate);
router.delete('/:cpf', controller.delete);

router.get('/', controller.getAll);


module.exports = router;
