'use strict';

const mongoose = require('mongoose');

const Worker = mongoose.model('Worker');

exports.createOrUpdate = async (data) => {
    const _worker = new Worker(data);
    const res = await Worker.findOneAndUpdate({
            CPF: data.CPF
        }, _worker, {
            upsert: true,
            new: true,
            runValidators: true
        },
        function (err, doc) {
            if (err) {
                throw err;
            } else {
                return _worker;
            }
        }
    );
    return res;
};

exports.getByName = async (name) => {
    const res = await Worker.find({
        Name: {
            $regex: name,
            $options: 'i'
        }
    });
    return res;
};

exports.getByCPF = async (cpf) => {
    const res = await Worker.find({
        CPF: {
            $regex: cpf,
            $options: 'i'
        }
    });
    return res;
};

exports.getByJob = async (job) => {
    const res = await Worker.find({
        Job: {
            $regex: job,
            $options: 'i'
        }
    });
    return res;
};

exports.getByRegistration = async (registration) => {
    var date = new Date(registration);
    var enddate = new Date(registration);
    enddate.setDate(enddate.getDate() + 1)
    const res = await Worker.find({
        $and: [{
            RegistrationDate: {
                $gte: date
            }
        }, {
            RegistrationDate: {
                $lte: enddate
            }
        }]
    });
    return res;
};


exports.countByUF = async (uf) => {
    const res = await Worker.find({
        UF: uf
    });
    return res.length;
};

exports.getBySalary = async (lowest, highest) => {
    const res = await Worker.find({
        $and: [{
                Salary: {
                    $gte: lowest
                }
            },
            {
                Salary: {
                    $lte: highest
                }
            }
        ]
    });
    return res;
};

exports.getBySalaryLowest = async (lowest) => {
    const res = await Worker.find({
        Salary: {
            $gte: lowest
        }
    });
    return res;
};

exports.getByStatus = async (status) => {
    const res = await Worker.find({
        Status: status.toUpperCase()
    });
    return res;
};

exports.delete = async (cpf) => {
    const res = await Worker.findOneAndDelete({
        CPF: cpf
    }, function (err, doc) {
        if (err) {
            throw err;
        } else {
            return doc;
        }
    })
};

exports.getAll = async (name) => {
    const res = await Worker.find();
    return res;
};