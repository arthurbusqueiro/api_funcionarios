'use strict';

const fs = require('fs');
const repository = require('../repositories/workers');
const utils = require('../utils/utils');
const Validators = require('../utils/validators');

exports.getByName = async (req, res, next) => {
  try {
    const data = await repository.getByName(req.params.name);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getByCPF = async (req, res, next) => {
  try {
    const data = await repository.getByCPF(req.params.cpf);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getByJob = async (req, res, next) => {
  try {
    const data = await repository.getByJob(req.params.job);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getBySalary = async (req, res, next) => {
  try {
    const data = await repository.getBySalary(req.params.lowest, req.params.highest);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getBySalaryLowest = async (req, res, next) => {
  try {
    const data = await repository.getBySalaryLowest(req.params.lowest);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getByRegistration = async (req, res, next) => {
  try {
    const data = await repository.getByRegistration(req.params.registration);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.getByStatus = async (req, res, next) => {
  try {
    const data = await repository.getByStatus(req.params.status);
    res.status(200).send({
      workers: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.countByUF = async (req, res, next) => {
  try {
    const data = await repository.countByUF(req.params.uf);
    res.status(200).send({
      count: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.creatOrUpdate = async (req, res, next) => {
  try {
    const validator = new Validators();
    validator.isRequired(req.body.Name, 'Informe o nome do Funcionário');
    validator.isRequired(req.body.CPF, 'Informe o CPF do Funcionário');
    validator.isRequired(req.body.Job, 'informe o Cargo do Funcionário');
    validator.isRequired(req.body.Salary, 'Informe o Salário do Funcionário');
    validator.isRequired(req.body.RegistrationDate, 'Informe a Data de Cadastro do Funcionário');
    validator.isRequired(req.body.UF, 'Informe a UF de Nascimento do Funcionário');
    validator.IsFixedLength(req.body.CPF, 11, 'O CPF deve conter 11 dígitos');
    validator.isNumbersOnly(req.body.CPF, 'O CPF deve conter apenas números');
    validator.isNumbersOnly(req.body.Salary, 'O Salário deve conter apenas números');
    validator.isCPF(req.body.CPF, 'O CPF não é válido');
    validator.isDate(req.body.RegistrationDate, 'A data não é válida');
    validator.isNotAfterToday(req.body.RegistrationDate, 'A data não pode ser no futuro');
    // Se os dados forem inválidos
    if (!validator.isValid()) {
      res.status(400).send({
        message: validator.errors()
      });
      return;
    }
    req.body.Status = req.body.Status.toUpperCase();
    const data = await repository.createOrUpdate(req.body);
    res.status(200).send({
      worker: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    const validator = new Validators();
    validator.isRequired(req.params.cpf, 'Informe o CPF do Funcionário');
    validator.IsFixedLength(req.params.cpf, 11, 'O CPF deve conter 11 dígitos');
    validator.isNumbersOnly(req.params.cpf, 'O CPF deve conter apenas números');
    validator.isCPF(req.params.cpf, 'O CPF não é válido');
    // Se os dados forem inválidos
    if (!validator.isValid()) {
      res.status(400).send({
        message: validator.errors()
      });
      return;
    }

    const data = await repository.delete(req.params.cpf);
    res.status(200).send({
      worker: data
    });
  } catch (error) {
    res.status(500).send({
      message: 'CPF não encontrado'
    });
  }
};

exports.getAll = async (req, res, next) => {
  try {
    const data = await repository.getAll();
    res.status(200).send({
      workers: data
    })
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};