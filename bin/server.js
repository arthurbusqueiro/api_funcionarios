var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
Task = require('../models/worker'), //created model loading here
bodyParser = require('body-parser');
const config = require('../config');

var swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('../swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// app.use('/api/v1', router);

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(config.connectionString, { useNewUrlParser: true });


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


const funcionariosRoute = require('../routes/workers');
const indexRoute = require('../routes/index');

app.listen(port);

app.use('/', indexRoute);
app.use('/workers', funcionariosRoute);


console.log('todo list RESTful API server started on: ' + port);