var should = require("should");
var request = require("request");
var chai = require("chai");
var expect = chai.expect;
var urlBase = "http://localhost:3000/";

describe("Teste API Funcionários", function () {
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var text = "";
    for (let index = 0; index < 10; index++) {
        text = "";
        for (var i = 0; i < 3; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        it("Serviço que localiza/retorna funcionários pelo Nome " + text, function (done) {
            request.get({
                    url: urlBase + "workers/name/" + text
                },
                function (error, response, body) {

                    var _body = {
                        workers: []
                    };
                    try {
                        _body = JSON.parse(body);
                    } catch (e) {
                        _body = {
                            workers: []
                        };
                    }
                    expect(response.statusCode).to.equal(200);

                    _body.workers.forEach(element => {
                        expect(element.Name).to.contain('Aaron');
                        expect(element.Name).to.be.a('string');
                        expect(element.CPF).to.be.a('string');
                        expect(new Date(element.RegistrationDate)).to.be.a('date');
                    });

                    done();
                }
            );
        });

    }


    it("Serviço que localiza/retorna funcionários pelo Nome Arthur", function (done) {
        request.get({
                url: urlBase + "workers/name/Arthur"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);

                _body.workers.forEach(element => {
                    expect(element.Name).to.contain('Arthur');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    var numbers = '1234567890';
    for (index = 0; index < 10; index++) {
        text = "";
        for (i = 0; i < 4; i++)
            text += numbers.charAt(Math.floor(Math.random() * numbers.length));
        it("Serviço que localiza/retorna funcionários pelo CPF " + text, function (done) {
            request.get({
                    url: urlBase + "workers/cpf/364"
                },
                function (error, response, body) {

                    var _body = {
                        workers: []
                    };
                    try {
                        _body = JSON.parse(body);
                    } catch (e) {
                        _body = {
                            workers: []
                        };
                    }
                    expect(response.statusCode).to.equal(200);
                    _body.workers.forEach(element => {
                        expect(element.CPF).to.contain('364');
                        expect(element.Name).to.be.a('string');
                        expect(element.CPF).to.be.a('string');
                        expect(new Date(element.RegistrationDate)).to.be.a('date');
                    });

                    done();
                }
            );
        });
    }
    it("Serviço que localiza/retorna funcionários pelo Cargo", function (done) {
        request.get({
                url: urlBase + "workers/job/Dev Jr"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);

                _body.workers.forEach(element => {
                    expect(element.Job).to.contain('Dev Jr');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários pela data de cadastro", function (done) {
        request.get({
                url: urlBase + "workers/registration/2017-04-08"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(new Date(element.RegistrationDate).getTime()).to.be.equal((new Date(2017, 3, 8).getTime()));
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    var ufs = ['AC',
        'AL',
        'AP',
        'AM',
        'BA',
        'CE',
        'DF',
        'ES',
        'GO',
        'MA',
        'MT',
        'MS',
        'MG',
        'PA',
        'PB',
        'PR',
        'PE',
        'PI',
        'RJ',
        'RN',
        'RS',
        'RO',
        'RR',
        'SC',
        'SP',
        'SE',
        'TO'
    ];

    var workers = 0;
    ufs.forEach(uf => {

        it("Serviço que retorna funcionários agrupados por UF de Nascimento, de forma quantitativa de " + uf, function (done) {
            request.get({
                    url: urlBase + "workers/uf/" + uf
                },
                function (error, response, body) {

                    var _body = {
                        workers: []
                    };
                    try {
                        _body = JSON.parse(body);
                    } catch (e) {
                        _body = {
                            workers: []
                        };
                    }
                    expect(response.statusCode).to.equal(200);
                    expect(_body.count).to.be.a('number');
                    workers += _body.count;
                    done();
                }
            );
        });
    });


    it("Serviço que localiza/retorna funcionários por faixa salarial 0 a 1000", function (done) {
        request.get({
                url: urlBase + "workers/salary/0/1000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(1000);
                    expect(element.Salary).to.be.least(0);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial 1000 a 5000", function (done) {
        request.get({
                url: urlBase + "workers/salary/1000/5000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(5000);
                    expect(element.Salary).to.be.least(1000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial 5000 a 20000", function (done) {
        request.get({
                url: urlBase + "workers/salary/5000/20000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(20000);
                    expect(element.Salary).to.be.least(5000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por faixa salarial maior que 20000", function (done) {
        request.get({
                url: urlBase + "workers/salary/20000"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Salary).to.be.most(20000);
                    expect(element.Salary).to.be.least(5000);
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });

    var workerStatus = 0;
    it("Serviço que localiza/retorna funcionários por Status Ativo", function (done) {
        request.get({
                url: urlBase + "workers/status/ativo"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.be.equal('ATIVO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });
                workerStatus += _body.workers.length;

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por Status Inativo", function (done) {
        request.get({
                url: urlBase + "workers/status/inativo"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.be.equal('INATIVO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });
                workerStatus += _body.workers.length;

                done();
            }
        );
    });
    it("Serviço que localiza/retorna funcionários por Status Bloqueado", function (done) {
        request.get({
                url: urlBase + "workers/status/bloqueado"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }

                expect(response.statusCode).to.equal(200);
                _body.workers.forEach(element => {
                    expect(element.Status).to.be.equal('BLOQUEADO');
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });
                workerStatus += _body.workers.length;

                done();
            }
        );
    });
    it("Serviço para incluir um novo funcionário", function (done) {
        request.post({
                url: urlBase + "workers",
                json: {
                    "CPF": "36448704892",
                    "Job": "PO Sr",
                    "Name": "Arthur Busqueiro",
                    "RegistrationDate": "2019-01-18T03:00:00.000+0000",
                    "Salary": 15000,
                    "Status": "BLOQUEADO",
                    "UF": "MG"
                }
            },
            function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                expect(body.worker.Status).to.be.equal("BLOQUEADO");
                expect(body.worker.Name).to.be.equal("Arthur Busqueiro");
                expect(body.worker.CPF).to.be.equal("36448704892");
                expect(body.worker.Job).to.be.equal("PO Sr");
                expect(new Date(body.worker.RegistrationDate).getTime()).to.be.equal(new Date("2019-01-18T03:00:00.000+0000").getTime());
                expect(body.worker.Salary).to.be.equal(15000);
                expect(body.worker.UF).to.be.equal("MG");

                done();
            });
    });

    it("Serviço para atualizar um novo funcionário com dados incorretos", function (done) {
        request.post({
                url: urlBase + "workers",
                json:  {
                    "CPF": "11655588866546",
                    "Job": "",
                    "Name": "Arthur",
                    "RegistrationDate": "2019-02-18T03:00:00.000+0000",
                    "Salary": 'SAS',
                    "Status": "III",
                    "UF": "ASD"
                }
            },
            function (error, response, body) {

                expect(response.statusCode).to.equal(400);
                expect(body.message.length).to.be.least(1);

                done();
            });
    });

    var workersAll = 0;
    it("Serviço que localiza/retorna todos os funcionários", function (done) {
        request.get({
                url: urlBase + "workers/"
            },
            function (error, response, body) {

                var _body = {
                    workers: []
                };
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {
                        workers: []
                    };
                }
                expect(response.statusCode).to.equal(200);
                expect(_body.workers.length).to.equal(workers);
                expect(_body.workers.length).to.equal(workerStatus);
                expect(workers).to.equal(workerStatus);
                _body.workers.forEach(element => {
                    expect(element.Name).to.be.a('string');
                    expect(element.CPF).to.be.a('string');
                    expect(new Date(element.RegistrationDate)).to.be.a('date');
                });

                done();
            }
        );
    });
});